# Role Name

This role will create vm instance of [Google Cloud Free Tier][gcloud-free-tier]


## Requirements

Ansible gcp module needs the following python packages to be installed locally:

    requests >= 2.18.4
    google-auth >= 1.3.0

Install them manually or install virtualenv package and use [tests/prepare-localhost.yml][tests/prepare-localhost.yml] to install them to `~/.virtualenv/pihole.`

Before you can use this role you need to:
  * Have google account.
  * Create one project ([click][gcloud-create-project]).
  * Enable project-wide ssh key acces for user **ansible** ([click][gcloud-project-wide-ssh]). 
      > Notes:
      >     * user to login as must be specified as the last string of the ssh-key ([click][gcloud-project-wide-ssh-user])
      >     * this user must be specified directly in playbook using your gcloud instance as `remote_user` or exported to env as `ANSIBLE_REMOTE_USER`
  * Create service account in this project ([click][gcloud-sa-create])
    and get json with credentials file. Put these credentials to playbook variable:

          vars:
              vault_gcp_service_account_contents: >
                  {
                      "type": "service_account",
                      "project_id": "project",
                      "private_key_id": "not-a-real-key-id",
                      "private_key": "-----BEGIN PRIVATE KEY-----\nnot-a-real-key",
                      "client_email": "sa-user@project.iam.gserviceaccount.com",
                      "client_id": "1",
                      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                      "token_uri": "https://oauth2.googleapis.com/token",
                      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/sa-user@project.iam.gserviceaccount.com"
                   }

    or put in a `vars/vault.yml` as `vault_gcp_service_account_content`


Role Variables
--------------

Check [default/main.yml][default/main.yml] for the most up-to-date list of variables.

Dependencies
------------

No dependencies.


Example Playbook
----------------

Check [tests/main.yml][tests/main.yml] for example playbook.

Show variables values:

    $ ansible-playbook -i test/inventory tests/create.yml -v -t show-vars

To create new instance:

    $ ansible-playbook -i test/inventory tests/create.yml

To destroy the same instance:

    $ ansible-playbook -i test/inventory tests/destroy.yml

License
-------

BSD

Author Information
------------------

No.

[gcloud-free-tier]: https://cloud.google.com/free/docs/gcp-free-tier
[gcloud-sa-create]: https://cloud.google.com/iam/docs/creating-managing-service-accounts#iam-service-accounts-create-console
[gcloud-create-project]: https://console.cloud.google.com/projectcreate
[gcloud-project-wide-ssh]: https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys?hl=en_US#project-wide
